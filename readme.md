## MQTT Server

This app will read `stdin` and takes the first number in the line and post it to Azure IoT Hub via MQTT, and will post to external 'Dripper' server

To run:

Install
`npm install`

To test
`npm run dev`

To run for real pipe your output to this process.
If your output device is USB (port 0)
`tail -f /dev/ttyUSB0 | npm run start`