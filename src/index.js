const azureConnection =
  "HostName=IoT-Cloud-Hub.azure-devices.net;DeviceId=Group01;SharedAccessKey=AZ0z8VmlHedqhQ6O+pUI+GgFY2AH2GOs9nD5UhSC4l8=";
const Protocol = require("azure-iot-device-mqtt").Mqtt;
const Client = require("azure-iot-device").Client;
const Message = require("azure-iot-device").Message;
const http = require("http");

const client = Client.fromConnectionString(azureConnection, Protocol);

const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

var numberPattern = /\d+/g;

const handleConnection = async err => {
  if (err) {
    console.log(err);
  } else {
    console.log("Connected");
    client.on("message", msg => {
      console.log("message received");
    });

    rl.on("line", async msg => {
      const [temp] = msg.match(numberPattern);
      console.log("got temp:", temp);
      http.get(`http://51.68.128.220:8080/api/temp/${temp}`);
      const message = new Message(msg);

      client.sendEvent(message, (err, res) => {
        if (err) {
          console.error(err.toString());
        } else {
          console.log(res.constructor.name, "Message: ", message.getData());
        }
      });
    });
  }
};
client.open(handleConnection);
